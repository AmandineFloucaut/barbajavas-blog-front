import { Injectable, Input } from '@angular/core';
import { Post } from '../model/post';
import axios from 'axios';

@Injectable({
  providedIn: 'root'
})

export class PostsService {

  private apiUrl: string = "http://localhost:8080/articles"

  public posts: Post[] = [];
  public post?: Post;

  constructor() { }

  // Posts list
  public async getListArticles(){
    const { data } = await axios.get(this.apiUrl);
    this.posts = data;
  }

  // Post Add
  public addNewArticle(post: Post){

    axios.post(this.apiUrl, post);
  }

  // One post
  public getArticleById(id: string){

    axios.get(this.apiUrl + '/' + id).then(response => {
      console.log(response.data);

      this.post = response.data;
    });
  }

  // Post Edit
  public editArticle(post: Post){

    axios.put(this.apiUrl + '/' + this.post?.id, post);
  }

}
