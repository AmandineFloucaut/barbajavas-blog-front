import { Component, Input, OnInit } from '@angular/core';
import { Post } from './model/post';
import { PostsService } from './services/posts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'barbajavas-blog';

  @Input()
  public post?: Post;

  constructor(){
  }

  ngOnInit(){


  }
}
