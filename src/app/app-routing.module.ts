import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AddPostFormComponent } from './components/add-post-form/add-post-form.component';
import { EditPostFormComponent } from './components/edit-post-form/edit-post-form.component';
import { PostDetailsComponent } from './components/post-details/post-details.component';
import { PostsPageComponent } from './pages/posts-page/posts-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'posts', pathMatch: 'full'},
  { path: 'posts', component: PostsPageComponent},
  { path: 'posts/add', component: AddPostFormComponent},
  { path: 'posts/edit/:id', component: EditPostFormComponent},
  { path: 'posts/:id', component: PostDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
