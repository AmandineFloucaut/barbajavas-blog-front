import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PostDetailsComponent } from './components/post-details/post-details.component';
import { PostsPageComponent } from './pages/posts-page/posts-page.component';
import { AddPostFormComponent } from './components/add-post-form/add-post-form.component';
import { EditPostFormComponent } from './components/edit-post-form/edit-post-form.component';

@NgModule({
  declarations: [
    AppComponent,
    PostDetailsComponent,
    PostsPageComponent,
    AddPostFormComponent,
    EditPostFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
