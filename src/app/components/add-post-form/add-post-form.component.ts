import { Component, OnInit } from '@angular/core';
import { PostsService } from 'src/app/services/posts.service';
import { Post } from '../../model/post';

@Component({
  selector: 'app-add-post-form',
  templateUrl: './add-post-form.component.html',
  styleUrls: ['./add-post-form.component.scss']
})
export class AddPostFormComponent implements OnInit {
  public title: string = "";
  public summary: string = "";
  public  content: string = "";

  constructor(public postsService: PostsService) { }

  ngOnInit(): void {
  }

  public addNewPost(){
    console.log(this.title, this.summary, this.content);

    const newPost: Post = {
      id: "",
      title: this.title,
      summary: this.summary,
      content: this.content
    }

    this.postsService.addNewArticle(newPost);
  }

}
