import { Component, Input, OnInit } from '@angular/core';
import { Post } from 'src/app/model/post';
import { PostsService } from 'src/app/services/posts.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent implements OnInit {

  @Input()
  public post?: Post;
  private postId: any;

  constructor(public postsService: PostsService, private activatedRoute: ActivatedRoute,
    private router: Router,) {

  }

  ngOnInit(): void {

   this.activatedRoute.params.subscribe(params => {
      this.postsService.getArticleById(params['id'])
    });
  }

}
