import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/model/post';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-edit-post-form',
  templateUrl: './edit-post-form.component.html',
  styleUrls: ['./edit-post-form.component.scss']
})
export class EditPostFormComponent implements OnInit {
  public title: string = "";
  public summary: string = "";
  public  content: string = "";

  public postId: string = "";

  constructor(public postsService: PostsService) { }

  ngOnInit(): void {
  }

  public editPost(){
    console.log(this.title, this.summary, this.content);

    if(this.postsService.post){
      this.postId = this.postsService.post?.id;
    }

    const updatedPost: Post = {
      id: this.postId,
      title: this.title,
      summary: this.summary,
      content: this.content
    }

    this.postsService.editArticle(updatedPost);
  }

}
